
import matplotlib.pyplot as plt

from core import SlowCooker, Logger
from controllers import SimpleController, PIDController, AdvancedPIDController

##################
# SIM WRAPPER
##################

class Simulator( object ):

	def __init__( self, controller ):
		self.__controller = controller
		self.__cooker = SlowCooker( 0.4, 0.2 )

	def runSim( self, maxTime ):
		"""
			Run the simulation
		"""
		time = 0
		dt = 0.1
		while time < maxTime:
			self.__controller.control( self.__cooker )
			self.__cooker.compute( dt )
			time += dt

##################
# SIMPLE METRICS
##################

def computeOvershoot( series, targetTemp ):
	"""
		Overshoot from the target temp
	"""
	return max( series ) - targetTemp

def computeStability( series ):
	"""
		Crude estimate of the stability, the range of the last 20%
	"""
	subRange = series[int( len( series ) * ( 0.8 ) ):]
	return max( subRange ) - min( subRange )

def computeTimeToTarget( series, target ):
	"""
		Compute time to target temp
	"""
	for i, value in enumerate( series ):
		if value > target:
			return i
	return len( series )

##################
# TESTS
##################

def testSimpleController():
	logger = Logger( 0.1 )
	controller = SimpleController( logger, targetTemp = 3 )
	sim = Simulator( controller = controller )
	sim.runSim( 30 )
	logger.plot()

def testPIDController():
	logger = Logger( 0.1 )
	controller = PIDController(
		logger,
		targetTemp = 3,
		P = 1,
		I = 0,
		D = -0.4 )
	sim = Simulator( controller = controller )
	sim.runSim( 30 )
	logger.plot()

def testAdvancedPIDController():
	logger = Logger( 0.1 )
	controller = AdvancedPIDController(
		logger,
		targetTemp = 3,
		P = 1,
		I = 0,
		D = -0.4 )
	sim = Simulator( controller = controller )
	sim.runSim( 30 )
	logger.plot()

##################
# OPTIMISE D
##################

def plotDerivative():

	ds = []
	os = []
	ts = []
	ss = []

	d = -1
	while d < 1:
		logger = Logger( 0.1 )
		controller = PIDController(
			logger,
			targetTemp = 3,
			P = 1,
			I = 0,
			D = d
			)
		sim = Simulator( controller = controller )
		sim.runSim( 30 )

		ds.append( d )
		os.append( computeOvershoot( logger.getDataSeries("temperature"), 3 ) )
		ss.append( computeStability( logger.getDataSeries("temperature") ) )
		ttt = computeTimeToTarget( logger.getDataSeries("temperature"), 3 )
		ts.append( ttt )

		d += 0.01

	# plot normalised values
	plt.plot(
		ds,
		[s/max(stability) for s in stability ],
		label = "stability"
		)
	plt.plot(
		ds,
		[o/max(overshoot) for o in overshoot ],
		label = "overshoot"
		)
	plt.plot(
		ds,
		[t/max(timeToTarget) for t in timeToTarget ],
		label = "time to target"
		)

	plt.legend()
	plt.show()

##################
# OPTIMIZE I
##################

def plotIntegral():

	derivatives = []
	overshoot = []
	stability = []
	timeToTarget = []

	i = -0.5
	while i < 0.5:
		logger = Logger( 0.1 )
		controller = PIDController(
			logger,
			targetTemp = 3,
			P = 1,
			I = i,
			D = 0.4
			)
		sim = Simulator( controller = controller )
		sim.runSim( 30 )

		derivatives.append( i )
		temperatureSeries = logger.getDataSeries( "temperature" )
		overshoot.append( computeOvershoot( temperatureSeries, 3 ) )
		stability.append( computeStability( temperatureSeries ) )
		timeToTarget.append( computeTimeToTarget( temperatureSeries, 3 ) )

		i += 0.01

	# plot normalised values
	plt.plot(
		derivatives,
		[s/max(stability) for s in stability ],
		label = "stability"
		)
	plt.plot(
		derivatives,
		[o/max(overshoot) for o in overshoot ],
		label = "overshoot"
		)
	plt.plot(
		derivatives,
		[t/max(timeToTarget) for t in timeToTarget ],
		label = "time to target"
		)

	plt.legend()
	plt.show()

if __name__ == "__main__":
	#testSimpleController()
	#testPIDController()
	testAdvancedPIDController()
	#plotDerivative()
	#plotIntegral()

