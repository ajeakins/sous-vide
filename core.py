
##################
# COOKER
##################

class SlowCooker( object ):
	"""
		This object represents a slow cooker with the
		where thermocouple measures the temperature of
		the bath.

		The rate of heat gain of the heater is assumed to be
		unity so the rateOfHeatGain is relative rate of transfer
		from the heater to the bath and rateOfHeatLoss is the
		relative rate of transfer of heat from the bath to the
		environment.
	"""
	def __init__( self, rateOfHeatGain, rateOfHeatLoss ):
		self.__heaterTemp = 1.0
		self.__bathTemp = 1.0

		self.__rateOfHeatGain = rateOfHeatGain
		self.__rateOfHeatLoss = rateOfHeatLoss

		self.__isOn = False

	def setOn( self, on ):
		"""
			Turn the heating element on/off.
		"""
		self.__isOn = bool( on )

	def measureTemp( self ):
		"""
			Measure the temperature seen at the thermocouple.
		"""
		return self.__bathTemp

	def compute( self, timeStep ):
		"""
			Intergrate one time step.
		"""
		# heater
		if self.__isOn:
			self.__heaterTemp += timeStep

		self.__heaterTemp -= self.__rateOfHeatGain \
			* ( self.__heaterTemp - self.__bathTemp ) * timeStep

		# bath
		self.__bathTemp +=  self.__rateOfHeatGain \
			* ( self.__heaterTemp - self.__bathTemp ) * timeStep
		self.__bathTemp -= self.__rateOfHeatLoss \
			* ( self.__bathTemp - 1 ) * timeStep

##################
# LOGGER
##################

class Logger( object ):
	"""
		Simple logger
	"""

	dt = property( lambda self: self.__dt )

	def __init__( self, dt ):
		self.__dt = dt
		self.__data = {}
		self.__time = [0]

	def __incTime( self ):
		self.__time.append( self.__time[-1] + self.__dt )

	def logDataPoint( self, key, value ):
		if not key in self.__data:
			self.__data[key] = []
		self.__data[key].append( value )

		if len( self.__time ) < len( self.__data[key] ):
			self.__incTime()

	def plot( self ):
		import matplotlib.pyplot as plt

		for key, values in self.__data.iteritems():
			plt.plot( self.__time, values, label = key )
		plt.legend()
		plt.show()

	def getDataSeries( self, key ):
		return self.__data[key]


