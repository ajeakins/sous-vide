
##################
# BASE
##################

class BaseController( object ):
	"""
		Controller Base Class
	"""

	targetTemp = property( lambda self: self.__targetTemp )
	logger = property( lambda self: self.__logger )

	def __init__( self, logger, targetTemp ):
		self.__targetTemp = targetTemp
		self.__logger = logger

	def control( self, cooker ):
		raise NotImplementedError()

##################
# SIMPLE
##################

class SimpleController( BaseController ):
	"""
		Simple controller, just implements the proportional term
	"""

	def control( self, cooker ):
		if cooker.measureTemp() < self.getTargetTemp():
			cooker.setOn( True )
		else:
			cooker.setOn( False )

		self.logger.logDataPoint( "temperature", cooker.measureTemp() )

##################
# PID
##################

class PIDController( BaseController ):
	"""
		PID controller
	"""

	P = property( lambda self: self.__P )
	I = property( lambda self: self.__I )
	D = property( lambda self: self.__D )

	def __init__( self, logger, targetTemp, P, I, D ):
		BaseController.__init__( self, logger, targetTemp )

		# controller constants
		self.__P = P
		self.__I = I
		self.__D = D

		# for calculating derivative
		self.__previousTemp = 1

		# keep a count on the integral term
		self.__totalError = 0

	def control( self, cooker ):
		self._control( cooker, self.__P, self.__I, self.__D )

	def _control( self, cooker, P, I, D ):
		# proportional term
		proportional = P * ( self.targetTemp - cooker.measureTemp() )
		error = proportional

		# derivative term
		delta = ( cooker.measureTemp() - self.__previousTemp ) / self.logger.dt
		derivative = D * delta
		error += derivative

		# integral term
		diff = ( cooker.measureTemp() - self.targetTemp ) * self.logger.dt
		self.__totalError += ( diff * I )
		error += self.__totalError

		if error > 0:
			cooker.setOn( True )
		else:
			cooker.setOn( False )

		self.__previousTemp = cooker.measureTemp()

		self.logger.logDataPoint( "temperature", cooker.measureTemp() )
		self.logger.logDataPoint( "derivative", derivative )
		self.logger.logDataPoint( "integral", self.__totalError )

##################
# PID IMPROVED
##################

class AdvancedPIDController( PIDController ):
	"""
		PID controller, the integral term isn't used until we
		reach the target temp to prevent a large acumulation.
	"""

	def __init__( self, logger, targetTemp, P, I, D ):
		PIDController.__init__( self, logger, targetTemp, P, I, D )

		self.__useIntegral = False

	def control( self, cooker ):

		# do we use the integral term
		if not self.__useIntegral and cooker.measureTemp() >= self.targetTemp:
			self.__useIntegral = True

		# apply integral term
		if self.__useIntegral:
			self._control( cooker, self.P, self.I, self.D )
		else:
			self._control( cooker, self.P, 0, self.D )

