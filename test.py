
import unittest

from core import SlowCooker, Logger
from controllers import SimpleController, PIDController, AdvancedPIDController

class Tests( unittest.TestCase ):

	def test_Stability( self ):
		"""
			Temperature shouldn't change while
			cooker is off
		"""
		cooker = SlowCooker( 0.4, 0.2 )

		for i in xrange( 0, 1000 ):
			cooker.compute( 0.1 )

		self.assertEqual( cooker.measureTemp(), 1 )

	def test_Cooling( self ):
		"""
			Make sure after a period of heating the
			cooker cools back to euqilibrium
		"""
		# Fast heating and cooling coefficients
		cooker = SlowCooker( 0.9, 0.8 )
		cooker.setOn( True )

		for i in xrange( 0, 100 ):
			cooker.compute( 0.01 )

		cooker.setOn( False )

		for i in xrange( 0, 5000 ):
			cooker.compute( 0.01 )

		self.assertAlmostEqual( cooker.measureTemp(), 1 )

if __name__ == "__main__":
	unittest.main()

